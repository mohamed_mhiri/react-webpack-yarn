import React from 'react'
import ReactDOM from 'react-dom'
import AppForm from './../components/AppForm.jsx'

ReactDOM.render(
    <AppForm/>,
    document.getElementById('root')
)