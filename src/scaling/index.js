import React, { Component } from 'react'

import ReactDOM from 'react-dom'
import AppComponent from './../components/AppComponent.jsx'

ReactDOM.render(
    <AppComponent/>,
    document.getElementById('root')
)
