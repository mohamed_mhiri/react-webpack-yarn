import React, { Component } from 'react'

export default class SelectInput extends Component {
    constructor(props) {
        super(props)
        this.handleSelectChange = this.handleSelectChange.bind(this)
        this.state= {
            selectedValue: [
                'node'
            ]
        }
    }

    handleSelectChange(event) {
        console.log(event.target.value);
        
        this.setState({
            selectedValue: event.target.value
        })
    }

    render() {
        return (
            <form>
                <select
                    multiple= {
                        true
                    }
                    value={
                        this.state.selectedValue
                    }
                    onChange={
                        this.handleSelectChange
                    }>
                    <option value="ruby">Ruby</option>
                    <option value="node">Node</option>
                    <option value="python">Python</option>
                </select>
            </form>
        )
    }
}