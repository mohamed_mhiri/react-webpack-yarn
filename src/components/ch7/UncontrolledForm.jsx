import React, { Component } from 'react'
import ReactDOM from 'react-dom'

export default class UncontrolledForm extends Component {
    constructor(props) {
        super(props)
        this.prompt = 'text'
        this.submit = this.submit.bind(this)
    }

    submit(event) {
        let emailAddress = this.refs.emailAddress
        console.log(ReactDOM.findDOMNode(emailAddress).value)        
    }

    render() {
        return (
            <div className="well">
                <p>{this.prompt}</p>
                <div className="form-group">
                    <input 
                    type="text"
                    ref="emailAddress"
                    className="form-control"
                    placeholder="mail@mail.com"/>
                </div>
                <div className="form-group">
                    <a 
                        value="Submit" 
                        className="btn btn-primary"
                        onClick={
                            this.submit
                        }
                        >
                        Submit
                    </a>
                </div>
            </div>
        )
    }
}