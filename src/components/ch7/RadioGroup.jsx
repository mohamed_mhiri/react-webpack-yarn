import React, { Component } from 'react'

export default class RadioGroup extends Component {
    constructor(props) {
        super(props)
        this.handleRadio = this.handleRadio.bind(this)
        this.state= {
            radioGroup: {
                angular: false,
                react: true,
                vuejs: false
            }
        }
    }

    handleRadio(event) {
        let obj = {}
        obj[event.target.value]= event.target.checked
        this.setState({
            radioGroup: obj
        })
    }

    render() {
        return (
            <form>
                <input 
                    type="radio"
                    name="radioGroup"
                    value="angular"
                    checked={
                        this.state.radioGroup[
                            'angular'
                        ]
                    }
                    onChange={
                        this.handleRadio
                    }
                />
                <input 
                    type="radio"
                    name="radioGroup"
                    value="react"
                    checked={
                        this.state.radioGroup[
                            'react'
                        ]
                    }
                    onChange={
                        this.handleRadio
                    }
                />
                <input 
                    type="radio"
                    name="radioGroup"
                    value="vuejs"
                    checked={
                        this.state.radioGroup[
                            'vuejs'
                        ]
                    }
                    onChange={
                        this.handleRadio
                    }
                />
            </form>
        )
    }
}