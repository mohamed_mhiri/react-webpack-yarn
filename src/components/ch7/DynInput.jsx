import React, { Component } from 'react'

export default class DynInput extends Component {
    constructor(props) {
        super(props)
        this.handleCommentChange = this.handleCommentChange.bind(this)
        this.handleTitleChange = this.handleTitleChange.bind(this)
        this.state = {
                title: '',
                comment: ''
        }
    }

    handleTitleChange(event) {
        this.setState({
                title: event.target.value,
                comment: this.state.comment
        })
    }

    handleCommentChange(event) {
        this.setState({
            comment: event.target.value.replace(/[0-9-_]/ig,'')
        })
    }

    render() {
        return (
            <form>
                <input
                    type="text"
                    name="title"
                    value={
                        this.state.title
                    }
                    onChange={
                        this.handleTitleChange
                    }
                />
                <input
                    type="text"
                    name="comment"
                    value={
                        this.state.comment
                    }
                    onChange={
                        this.handleCommentChange
                    }
                />
            </form>

        )
    }
}