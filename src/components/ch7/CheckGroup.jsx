import React, { Component } from 'react'

export default class checkBoxGroup extends Component {
    constructor(props) {
        super(props)
        this.handleRadio = this.handleRadio.bind(this)
        this.state= {
            checkBoxGroup: {
                angular: false,
                react: true,
                vuejs: false
            }
        }
    }

    handleRadio(event) {
        let obj = Object.assign(this.state.checkBoxGroup)
        obj[event.target.value]= event.target.checked
        this.setState({
            checkBoxGroup: obj
        })
    }

    render() {
        return (
            <form>
                <input 
                    type="checkbox"
                    name="checkBoxGroup"
                    value="angular"
                    checked={
                        this.state.checkBoxGroup[
                            'angular'
                        ]
                    }
                    onChange={
                        this.handleRadio
                    }
                />
                <input 
                    type="checkbox"
                    name="checkBoxGroup"
                    value="react"
                    checked={
                        this.state.checkBoxGroup[
                            'react'
                        ]
                    }
                    onChange={
                        this.handleRadio
                    }
                />
                <input 
                    type="checkbox"
                    name="checkBoxGroup"
                    value="vuejs"
                    checked={
                        this.state.checkBoxGroup[
                            'vuejs'
                        ]
                    }
                    onChange={
                        this.handleRadio
                    }
                />
            </form>
        )
    }
}