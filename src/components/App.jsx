/*
    ./src/components/App.jsx
*/
import React from 'react'
//import Clock from './old/clock/Clock.jsx'
//import Menu from './old/menu/Menu.jsx'
//import Antipattern from './old/Antipattern.jsx'
//import Content from './old/content/Content.jsx'
//import Users from './old/users/Users.jsx'
//import Note from './old/note/Note.jsx'
//import SaveButton from './old/button/Button.jsx'
//import Mouse from './old/mouse/Mouse.jsx'
//import Container from './old/container/Container.jsx'
//import Radio from './old/radio/Radio.jsx'
import SliderButtons from './old/slider-button/SliderButtons.jsx'
import SliderValue from './old/slider-value/SliderValue.jsx'

export default class App extends React.Component {
  constructor(props) {
    super(props)
    /*this.state = {
      secondsLeft: 0
    }
    this.timePlusPlus()*/
  }

  /*timePlusPlus() {
    setInterval(()=>{
        console.log('Updating time...')
        this.setState({
          secondsLeft: ++this.state.secondsLeft
        })
    }, 1000)
  }*/

  render() {
    return (
      //<Clock/>,
      //<Menu  item1="item1" item2="item2" item3="item3" ></Menu>
      //<Users url = 'https://jsonplaceholder.typicode.com/posts'/>
      //<Note secondsLeft={this.state.secondsLeft}/>
      //<SaveButton/>
      //<Mouse/>
      //<Container/>
      <div>
        <SliderValue />
        <SliderButtons />
      </div>
    )
  }
}