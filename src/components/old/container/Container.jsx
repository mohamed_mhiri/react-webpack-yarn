import React, { Component } from 'react'

import ClickCounterButton from './../button/ClickCounterButton.jsx'
import Counter from './../counter/Counter.jsx'

export default class Container extends Component {
    constructor(props) {
        super(props)
        this.handleClick = this.handleClick.bind(this)
        this.state = {
            counter: 0
        }
    }

    handleClick(event) {
        this.setState({
            counter: 
            this.state.counter + 1
        })
    }

    render() {
        return (
            <div>
                <ClickCounterButton
                    
                    handler = {this.handleClick}>
                </ClickCounterButton>
                <Counter
                    counter = {this.state.counter}>
                </Counter>
            </div>
            
        )
    }
}