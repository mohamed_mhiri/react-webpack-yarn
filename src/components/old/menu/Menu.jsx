import React, { Component } from 'react'

export default class Menu extends Component {
    constructor(props) {
        super(props)
    }

    componentWillMount() {
        console.log('component will mount...')
    }

    componentDidlMount(e) {
        console.log('component did mount...')
        console.log('DOM node: ', ReactDOM.findDOMNode(this))
    }

    componentWillUpdate(newProps, newState) {
        console.log('component will update...')
        console.log('new props: ', newProps)
        console.log('new state: ', newState)
    }
    componentDidUpdate(oldProps, oldState) {
        console.log('component did update...')
        console.log('new props: ', oldProps)
        console.log('old props: ', oldState)
    }

    shouldComponentUpdate(newProps, newState) {
        console.log('shouldComponentUpdate is triggered')
        console.log('new props: ', newProps)
        console.log('new state: ', newState)
        return true
    }

    componentWillReceiveProps() {
        console.log('component will receive props...')
        console.log('new props: ', newProps)
    }

    componentDidCatch() {
        console.log('component did catch...')
    }

    componentWillUnmount() {
        console.log('component will unmount...')
    }

    render() {
        return (
            <ul>
                <li>{this.props.item1}</li>
                <li>{this.props.item2}</li>
                <li>{this.props.item3}</li>
            </ul>
        )
    }
}
