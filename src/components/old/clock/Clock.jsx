import React, { Component } from 'react'

export default class Clock extends Component {
    constructor(props) {
        super(props)
        this.launchClock()
        this.state = {currentTime: (new Date()).toLocaleString()}
    }
    launchClock() {
        setInterval(()=>{
            console.log('Updating time...')
            this.setState({
                currentTime: (new Date()).toLocaleString()
            })
        }, 1000)
    }    
    render() {
        return (
            <div>
                {this.state.currentTime}
                <span style={{
                    fontSize: "26px"
                }}>
                {
                    this.state.currentTime.split((":"))[2] == 0 ? 
                    <p style={{
                        color: "blue"
                    }}>ok</p> : <p style={{
                        color: "red"
                    }}>No</p>
                }
            </span>
            </div>
            
        )
    }
}