import React, { Component } from 'react'

export default class Counter extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <p>Clicked {this.props.counter} times</p>
        )
    }
}