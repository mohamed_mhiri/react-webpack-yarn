import React, { Component } from 'react'

export default class SaveButton extends Component {
    constructor(props) {
        super(props)
        this.handleSave = this.handleSave.bind(this)
    }

    handleSave(event) {
        console.log(this, event)
    }

    render() {
        return (
            <div>
                <button 
                    onClick={
                        this.handleSave
                    }
                    style={{
                        fontFamily: 'Ubuntu'
                    }}
                >Click Me!</button>
                <input 
                    type="text" 
                    onKeyPress={this.handleSave}
                    style={{
                        fontFamily: 'Ubuntu'
                    }}
                />
            </div>

        )
    }
}