import React, { Component } from 'react'

export default class ClickCounterButton extends Component {

    constructor(props) {
        super(props)
    }

    render() {
        return (
            <button
                onClick={this.props.handler}
                className="btn btn-dark">
                Click on me to increment the counter
            </button>
        )
    }
}