import React, { Component } from 'react'

export default class SliderButtons extends Component {
    constructor(props) {
        super(props)
        this.state = { sliderValue: 0 }
        let _this = this
    }

    handleSlide(event, ui) {
        this.setState({ sliderValue: ui.value })
    }

    handleChange(value) {
        
        return () => {
            $('#slider').slider('value', this.state.sliderValue + value)
            _this.setState({ sliderValue: this.state.sliderValue + value })
        }
    }

    componentDidMount() {
        let _this = this
        $('#slider').on('slide', _this.handleSlide)
    }

    componentWillUnmount() {
        $('#slider').off('slide', _this.handleSlide)
    }

    render() {
        return (
            <div>
                <button disabled={(this.state.sliderValue < 1) ? true : false}
                    className="btn default-btn"
                    onClick={this.handleChange(-1)}>
                    1 Less ({this.state.sliderValue - 1})
                </button>
                <button disabled={(this.state.sliderValue > 99) ? true : false}
                    className="btn default-btn"
                    onClick={this.handleChange(1)}>
                    1 More ({this.state.sliderValue + 1})
                </button>
            </div>
        )
    }
}