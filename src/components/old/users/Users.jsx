import React, { Component } from 'react'
import axios from 'axios'

export default class Users extends Component {
    constructor(props) {
        super(props)
        this.state = {
            users: []
        }
    }

    componentDidMount() {
        axios
        .get(this.props.url)
        .then(response => {
            this.setState({users: response.data})
        })
    }

    render() {
        return (
            <table className="table">
                <thead className="thead-dark">
                    <tr>
                        <th scope="col">UserId</th>
                        <th scope="col">Title</th>
                        <th scope="col">Body</th>
                    </tr>
                </thead>
                <tbody>
                        {
                            this.state.users
                            .filter(data => data.id <20)
                            .map(user => 
                                <tr key={user.id}>
                                    <th scope="row">{user.userId}</th>
                                    <td>{user.title}</td>
                                    <td>{user.body}</td>
                                </tr>
                            )
                        }
                </tbody>
            </table>
        )
    }
}