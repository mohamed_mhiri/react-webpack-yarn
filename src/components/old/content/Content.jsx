import React, { Component } from 'react'
import Logger from '../logger/Logger.jsx'

export default class Content extends Component {
    constructor(props) {
        super(props)
        this.launchClock()
        this.state = {
            counter: 0,
            currentTime: (new Date()).toLocaleString()
        }
        this.name = 'ff'
    }
    launchClock() {
        setInterval(() => {
            this.setState({
                counter: this.state.counter + 3,
                currentTime: (new Date()).toLocaleString()
            })
        }, 1000)
    }
    render() {
        if (this.state.counter > 10 && this.state.counter < 20) return <div> {this.name}</div>
        return <Logger time={this.state.currentTime}></Logger>
    }
}