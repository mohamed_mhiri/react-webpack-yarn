import React, { Component } from 'react'

const LoadWebsite = (Component) => {
    class _LoadWebsite extends React.Component {
        constructor(props) {
            super(props)
            this.state = {
                label: 'Run'
            }
            this.handleClick = this.handleClick.bind(this)
        }

        getUrl() {
            return 'https://facebook.github.io/react/docs/top-level-api.html'
        }

        handleClick(event) {
            let iFrame = document.getElementById('frame').src = this.getUrl()
        }

        componentDidMount() {
            console.log(ReactDOM.findDOMNode(this))
        }

        render() {
            console.log(this.state)
            return (
                <Component
                    {
                    ...this.state
                    }
                    {
                    ...this.props
                    }
                />
            )
        }
    }
    _LoadWebsite.displayName = 'EnhanceComponent'
    return _LoadWebsite
}
import Btn from './Btn.jsx'
import Link from './Link.jsx'
import Logo from './Logo.jsx'
const EnhancedButton = LoadWebsite(Btn)
const EnhancedLink = LoadWebsite(Link)
const EnhancedLogo = LoadWebsite(Logo)
class Content extends React.Component {
    render() {
        return (
            <div>
                <EnhancedButton />
                <br />
                <br />
                <EnhancedLink />
                <br />
                <br />
                <EnhancedLogo />
                <br />
                <br />
                <iframe id="frame" src="" width="600" height="500" />
            </div>
        )
    }
}