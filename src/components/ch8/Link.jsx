import React, { Component } from 'react'

export default class Link extends Component {
    render() {
        return <a onClick={this.props.handleClick} href="#">
        {this.props.label}</a>
        }
}