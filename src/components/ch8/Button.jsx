import React, { Component } from 'react'

export default class Button extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <button className="btn">
                {this.props.label}-{this.props.email}
            </button>
        )
    }
}

Button.propTypes = {
    label: PropTypes.string,
    email: function (props, propName, componentName) {
        var emailRegularExpression =
            /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
        if (!emailRegularExpression.test(props[propName])) {
            return new Error('Email validation failed!')
        }
    }
}
Button.defaultProps = {
    label: 'Submit',
    email: 'mail@mail.com'
}