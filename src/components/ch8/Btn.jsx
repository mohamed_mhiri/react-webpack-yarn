import React, { Component } from 'react'

class Btn extends Component {
    render() {
        return <button
            className="btn btn-primary"
            onClick={this.props.handleClick}>
            {this.props.label}
        </button>
    }
}