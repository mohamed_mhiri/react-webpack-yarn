import React from 'react'
// Anti-pattern: Stay away from it!
let inputValue = 'Texas'
export default class Antipattern extends React.Component {
    updateValues() {
        this.props.inputValue = 'California'
        inputValue = 'California'
        this.inputValue = 'California'
    }
    render() {
        return (
            <div>
                {this.props.inputValue}
                {inputValue}
                {this.inputValue}
            </div>
        )
    }
}