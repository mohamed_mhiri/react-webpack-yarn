# Notes

[Tuto](https://scotch.io/tutorials/setup-a-react-environment-using-webpack-and-babel#webpack-installation-and-configuration)

One way to add the bundled file to our HTML is to insert a ```<script>``` tag and pass the location of the bundled file into the script tag. A better way to do this is to use this nifty package called 
#### html-webpack-plugin
which provides an easy way to have all your HTML generated for you. All you need is the standard HTML skeleton and it'll take care of your script insertions with just a few configurations. Let's do that next.

## Upgrading to babel-preset-env
**babel-preset-env** is a new preset, first released over a year ago that replaces many presets that were previously used including:

babel-preset-es2015, babel-preset-es2016, babel-preset-es2017
babel-preset-latest
other community plugins involving es20xx:
babel-preset-node5, babel-preset-es2015-node, etc
instead of continuing yearly presets, the team recommends using babel-preset-env

Mutable means state values can change

As mentioned earlier (repetition is the mother of skills), properties will
change the view if you pass a new value from a parent, which in turn will create
a new instance of the component you’re currently working with. In the context
of a given component, changing properties as in ```this.props.inputValue = 'California'``` won’t cut it.

Changing a state value in your code using this.state.name= 'new name'
won’t do any good. This won’t trigger a rerender and a possible real DOM
update, which you want. For the most part, changing state directly without
setState() is an antipattern and should be avoided.

States and properties are both attributes of a class, meaning they’re this.state and
this.props . That’s the only similarity! One of the primary differences between states
and properties is that the former are mutable, whereas the latter are immutable.
Another difference between properties and states is that you pass properties from
parent components, whereas you define states in the component itself, not its parent. The philosophy is that you can only change the value of a property from the
parent, not the component. So properties determine the view upon creation, and
then they remain static (they don’t change). The state, on the other hand, is set and
updated by the object.
Properties and states serve different purposes, but both are accessible as attributes
of the component class, and both help you to compose components with a different
representation (view). There are differences between properties and states when it
comes to the component lifecycle (more in chapter 5). Think of properties and states
as inputs for a function that produces different outputs. Those outputs are views. So
you can have different UI s (views) for each set of different properties and states.

Stateless components are the preferred way of working with React.

## React Components' events:
Mounting—React invokes events only once.
Updating—React can invoke events many times.
Unmounting—React invokes events only once.

## React component lifecycle events:

1. ```componentWillMount()``` is invoked only once in the component’s lifecycle. The timing of the execution is just before the initial rendering.
The lifecycle event ```componentWillMount()``` is executed when you render a React
element on the browser by calling ```ReactDOM.render()``` . Think of it as attaching (or
mounting) a React element to a real DOM node. This happens in the browser: the
front end. If you render a React component on a server (the back end, using isomorphic/
universal JavaScript), which basically gets an HTML string, then—even
though there’s no DOM on the server or mounting in that case—this event will also be
invoked!
Updating the state can be triggered by calling ```setState()``` in the ```constructor()``` as well as in componentWillMount().
And ```render()``` will get the updated state. The best thing is that even if the new state is different, there will be no rerendering because
```render()``` will get the new state. To put it another way, you can invoke ```setState()``` in ```componentWillMount()```.
```render()``` will get the new values, if any, and there will be no extra rerendering.

2. ```componentDidMount()``` is invoked after the initial rendering. It’s executed only once and only in the browser, not on the server. This comes in handy when you need to implement code that runs only for browsers, such as XHR requests.
In this lifecycle event, you can access any references to children (for example, to access the corresponding DOM representation). Note that the ```componentDidMount()``` method of child components is invoked before that of parent components.

3. ```componentWillReceiveProps(newProps)``` is triggered when a component receives
new properties. This stage is called an incoming property transition. This event allows you
to intercept the component at the stage between getting new properties and before
render() , in order to add some logic. These properties may not necessarily have new
values (meaning values different from current properties), because React has no way
of knowing whether the property values have changed. Therefore, ```componentWillReceiveProps(NewProps)``` is invoked each time there’s a rerendering (of a parent
structure or a call), regardless of property-value changes. Thus, you can’t assume that
newProps always has values that are different from the current properties.
At the same time, rerendering (invoking render() ) doesn’t necessarily mean
changes in the real DOM . The decision whether to update and what to update in the
real DOM is delegated to ```shouldComponentUpdate()``` and the reconciliation process.

4. Next is the ```shouldComponentUpdate()``` event, which is invoked right before render-
ing. Rendering is preceded by the receipt of new properties or state.
The ```shouldComponentUpdate()``` event isn’t triggered for the initial render or for ```forceUpdate()```.
You can implement the ```shouldComponentUpdate()``` event with return false
to prohibit React from rerendering. This is useful when you’re checking that there
are no changes and you want to avoid an unnecessary performance hit (when
dealing with hundreds of components).

5. ```componentWillUpdate();``` is called just before rendering, pre-
ceded by the receipt of new properties or state. This method isn’t called for the initial
render. Use the componentWillUpdate() method as an opportunity to perform preparations before an update occurs,
 and avoid using this.setState() in this method else a new update will be triggered while the component is
being updated.
If ```shouldComponentUpdate()``` returns false, then ```componentWillUpdate()``` isn’t
invoked.

6. ```componentDidUpdate()``` event is triggered immediately after the component’s
updates are reflected in the DOM . Again, this method isn’t called for the initial render.
```componentDidUpdate()``` is useful for writing code that works with the DOM and its
other elements after the component has been updated, because at this stage you’ll get
all the updates rendered in the DOM .

7. unmounting means detaching or removing an element from the DOM.
There’s only ```componentWillUnmount``` in this category, and this is the last category in the component
lifecycle. ```componentWillUnmount``` is usefull to invalidate timers, clean up any DOM elements, or detach events that were created
in ```componentDidMount```.

## Working with DOM events in React

```bind()``` is needed so that in the event-handler function, you get a reference to the
instance of the class (React element). If you don’t bind, this will be null ( use strict
mode). You don’t bind the context to the class using ```bind(this)``` in the following
cases:
1. When you don’t need to refer to this class by using this
2. When you’re using the older style, ```React.createClass()``` , instead of the newer
ES6+ class style, because ```createClass()``` autobinds it for you
3. When you’re using fat arrows ( () => {} )

React exhaustive events list: https://reactjs.org/docs/events.html

React attaches events to document , not to each element. This allows React to be faster, especially when working
with lists. This is contrary to how jQuery works: with that library, events are attached to
individual elements. Kudos to React for thinking about performance.
If you have other elements with the same type of event for example, two mouseovers then they’re attached to one event and handled by React’s internal mapping
to the correct child (target element). 
And speaking of target elements, you can get information about the target node (where the event originated)
from the event object by typing ```getEventListeners(document)``` in the.

## Some Event 

currentTarget — DOMEventTarget of the element that’s capturing the event (can be a target or the parent of a target)
1. target — DOMEventTarget , the element where the event was triggered
2. nativeEvent — DOMEvent , the native browser event object
3. preventDefault() —Prevents the default behavior, such as a link or a form-
submit button
4. isDefaultPrevented() —A Boolean that’s true if the default behavior was
prevented
5. stopPropagation() —Stops propagation of the event
6. isPropagationStopped() —A Boolean that’s true if propagation was stopped
7. type —A string tag name
8. persist() —Removes the synthetic event from the pool and allows references
to the event to be retained by user code
9. isPersistent —A Boolean that’s true if SyntheticEvent was taken out of the
pool

## Working with Forms

The React documentation says, **"React components must represent the state of the view at any point in time and not only at initialization time."**

React is all about keeping things simple by using declarative style to describe UIs. React describes the UI : its end stage, how it should look.

Given these points, the best practice is to implement these things to sync the internal
state with the view:
1. Define elements in ```render()``` using values from state .
2. Capture changes to a form element as they happen, using ```onChange()``` .
3. Update the internal state in the event handler.
4. New values are saved in state , and then the view is updated by a new ```render()``` .

 ```
    render() {
        return <input type="text" name="title" value="Mr." />
    }
 ```
 must be changed to:
 ```
    handleChange(event) {
        this.setState({title: event.target.value})
    }
    render () {
        return (
            <input
                type="text"
                name="title"
                value={this.state.title}
                onChange={
                    this.handleChange.bind(this)
                }
            />
        )
    }
 ```

React uses **one-way** binding approach, which means that the state (or the model) won't be updated automatically. One of the main benefits of **one-way** binding is that it removes complexity when you’re working with large apps where many views implicitly can update many states (data models) and vice versa.

**One-way** binding is responsible for the **model to view**transition. **Two-way** binding also
handles changes from **view to model**.

**Controlled components** 's values are controlled, asigned by React.

As a reminder, don’t invoke a method ~~()~~ (don’t put parentheses) and
don’t use double quotes around curly braces ~~"{}"~~ (correct: **EVENT={this.METHOD}** ) when setting the event handler.

**event.keyCode == 13** refers to on Enter.

```<input>``` ,```<textarea>``` ,```<select>``` , and ```<option>``` are the four elements used to implement all input fields in HTML. Thus, React made these elements special by giving them the mutable properties value, checked ,and selected. These special mutable properties are also called interactive properties:

1. value —Applies to ```<input>```, ```<textarea>```, and ```<select>```
2. checked —Applies to ```<input>``` with ```type="checkbox"``` and ```type="radio"```
3. selected —Applies to ```<option>``` (used with ```<select>``` )

cloning objects can be done with: 
1. ```obj = Object.assign(...)```
2. ```obj = JSON.parse(JSON.stringify(...))```

**Uncontrolled components** are discouraged for the reasons that the view’s DOM state may be different than React’s internal state.

When you’re working with **controlled components** or with **uncontrolled components that capture data**, the data is in the state all the time. This
isn’t the case, if using **uncontrolled components without capturing changes**. Thus, the solution is using references to access uncontrolled components' values.

## Scaling React components

**Developmental scalability issues**: how to scale your code, meaning how to work with your code when the code base grows larger

React version: an uncompressed version for development
and a minified version for production. The development version includes extra warnings about common mistakes, whereas the production version includes extra performance optimizations and strips all error messages.

Never rely on front-end user-input validation, because it can be easily bypassed. Use it only for a better UX , and check everything on the server side.

[ReactJs proptypes](https://reactjs.org/docs/typechecking-with-proptypes.html#proptypes)

The ```React.Fragment``` component lets you return multiple elements in a ```render()``` method without creating an additional DOM element:
```
render() {
  return (
    <React.Fragment>
      Some text.
      <h2>A heading</h2>
    </React.Fragment>
  )
}
```

A ***higher-order component ( HOC )*** lets you enhance a component with additional logic, in other words, HOCs let you reuse code.

In essence, **HOC** s are React component classes that render the original classes while
adding extra functionality along the way. Defining an HOC is straightforward, because
it’s only a function. You declare it with a fat arrow:
```
    const LoadWebsite = (Component) => {
        ...
    }
```
The name LoadWebsite is arbitrary; you can name the HOC anything, as long as you
use the same name when you enhance a component. The same is true for the argument to the function ( LoadWebsite ); it’s the original (not yet enhanced) component.

